## gentoo-overlay

#### add this overlay:
root # `wget -P /etc/portage/repos.conf/ https://gitlab.com/graphonymous/gentoo-overlay/-/raw/master/graphonymous-overlay.conf` 

root # `emerge --sync`

---
#### credits

[dev-java/ 2xsaiko](https://git.sr.ht/~dblsaiko/ebuilds)

---
[Unlicense](LICENSE)
